print("MENU Registros\n\n1)-Nuevo\n2)-Mostrar\n3)-Eliminar Registros\n4)-Buscar")

opcion = input("Elige una opción: ")

if opcion == "1":
    print("Nuevo Registro\n")
    archivo = open("ejercicio2.csv","a")

    modelo = input("Ingrese el modelo: ")
    imei = input("Ingrese el imei: ")

    print("Se han capturado: " + modelo + ", con el tel: " + imei)

    archivo.write(modelo)
    archivo.write(",")
    archivo.write(imei)
    archivo.write(",")
    archivo.write("\n")

    archivo.close()

elif opcion == "2":
    print("Mostrar Registros")
    archivo = open("ejercicio2.csv")

    print(archivo.read())

    archivo.close()

elif opcion == "3":
    archivo = open("ejercicio2.csv","a")
    archivo.truncate()

    print("Registros Eliminados")

    archivo.close()

elif opcion == "4":
    archivo = open("ejercicio2.csv")

    archivo.close()
else:
    print("Debes de elegir una opcion anterior")